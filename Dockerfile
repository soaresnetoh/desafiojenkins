FROM ubuntu
RUN apt-get update
RUN apt-get install vim nano nginx -y
COPY ./site /var/www/html
ENTRYPOINT ["/usr/sbin/nginx"]
CMD ["-g", "daemon off;"]