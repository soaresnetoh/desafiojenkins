#!/bin/sh

echo "START" >> /tmp/status.txt
sudo chmod 777 /tmp/status.txt
# config jenkins installer on ubuntu
#sudo add-apt-repository ppa:webupd8team/java -y
sudo add-apt-repository ppa:linuxuprising/java -y

wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -

# config java8 installer on ubuntu
sudo sh -c 'echo "deb https://pkg.jenkins.io/debian binary/" >> /etc/apt/sources.list'
echo debconf shared/accepted-oracle-license-v1-1 select true | \
sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
sudo debconf-set-selections

echo "CONFIG DONE" >> /tmp/status.txt 
sudo apt-get update  >> /tmp/status.txt

# apt-get install java and jenkins
#sudo apt-get -y install oracle-java8-installer  >> /tmp/status.txt
echo oracle-java11-installer-local shared/accepted-oracle-license-v1-2 select true | sudo /usr/bin/debconf-set-selections
#sudo apt-get -y install oracle-java11-installer-local 2>/dev/null >> /tmp/status.txt
sudo apt-get -y install openjdk-11-jre-headless >> /tmp/status.txt

sudo apt-get -y install jenkins  >> /tmp/status.txt

echo "APT-GET INSTALL DONE" >> /tmp/status.txt

# wait for jenkins start up
sudo wget localhost:8080/jnlpJars/jenkins-cli.jar
response=""
key=""
while [ `echo $response | grep 'Authenticated' | wc -l` = 0 ]; do
  key=`sudo cat /var/lib/jenkins/secrets/initialAdminPassword`
  echo $key >> /tmp/status.txt
  echo "admin:$key" > creds
  response=`sudo java -jar jenkins-cli.jar -s http://localhost:8080 who-am-i`
  sudo echo $response
  sudo echo "Jenkins not started, wait for 2s"
  sleep 2
done >> /tmp/status.txt
echo "Jenkins started" >> /tmp/status.txt
echo "Install Plugins" >> /tmp/status.txt

# install plugins with jenkins-cli
for package in ant blueocean blueocean-autofavorite build-timeout email-ext \
    ghprb gradle jacoco workflow-aggregator pipeline-github-lib sbt ssh-slaves \
    subversion timestamper ws-cleanup; do sudo sh -c \
    "sudo java -jar jenkins-cli.jar -s http://localhost:8080/ -auth @creds \
    install-plugin $package  >> /tmp/status.txt"; done;

echo "PLUGINS INSTALL DONE" >> /tmp/status.txt


# restart jenkins
/etc/init.d/jenkins restart  >> /tmp/status.txt

echo "ALL DONE" >> /tmp/status.txt