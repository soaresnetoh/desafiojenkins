resource "aws_vpc" "poc-terraform-aws" {
  cidr_block = "10.99.0.0/16"
  instance_tenancy = "dedicated"
  tags = {
    Name = "poc-terraform-aws"
    projeto = "${var.instance_type}"
   }
}