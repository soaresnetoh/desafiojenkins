# Default Region
variable "region" {
  description = "AWS Region"
  default = "us-east-1"
}

# Profile AWS
variable "profile" {
  description = "AWS Profile"
  default = "terraform-lab"
}

# Caminho para a public key SSH
variable "key_path" {
  description = "Public key path"
  default = "~/.ssh/id_rsa.pub"
}

# Tipo da AMI que será utilizada para as EC2
variable "ami" {
  description = "AMI"
  #default = "ami-8c1be5f6" // Amazon Linux
  default = "ami-09e67e426f25ce0d7" //Ubuntu Server 20.04 LTS (HVM), SSD Volume Type - ami-09e67e426f25ce0d7 (64 bits x86) 
}

# Classe da instância que será utilizada
variable "instance_type" {
  description = "EC2 instance type"
  default = "t2.micro"
}

# Classe da instância que será utilizada
variable "tag_projeto" {
  description = "TAG Projeto"
  default = "lab-terraform-jenkins"
}