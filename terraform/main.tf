provider "aws" {
    profile = "${var.profile}"
    region = "${var.region}" 
}

# Security Group Configs
resource "aws_security_group" "default" {
  name = "sg_jenkins"
  
  # Liberar a porta 80 para acesso livre via Internet
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["177.12.40.132/32"]
  }
  


  # Liberar todo o tráfego de saida
  egress {
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



# Recurso Chaves de seguranca
resource "aws_key_pair" "my-key" {
  key_name   = "my-key"
  public_key = "${file("${var.key_path}")}"
}

resource "aws_instance" "jenkins" {
    ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    key_name = aws_key_pair.my-key.key_name
    security_groups = ["${aws_security_group.default.name}"]
    user_data = "${file("jenkins/install_jenkins.sh")}"
    tags = {
        "Name" = "Jenkins_Server"
        "Terraform" = "true"
        "projeto" = "${var.tag_projeto}"
    }
}

output "instance_dns" { 
  value = "${aws_instance.jenkins.public_dns}"
}