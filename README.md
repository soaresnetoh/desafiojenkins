<!-- ABOUT THE challenge -->
## Sorbre o desafio 

Neste desafio voce precisará criar uma estrutura de Esteira Devops "simples" com os segiuntes passos:

* criar um respositorio no gitlab para que seja feito commit de todas as alterações do desafio
* criar um Dockerfile composto de uma aplicação a sua escolha
* habilitar o webhook deste repositorio
* criar uma EC2 na AWS com o jenkins instalado
  
  --- Atraves do webhook, ao fazer o commit, o jenkins tem que :
  1- fazer o clone

  2- fazer o build

  3- enviar a imagem (artefato) para o dockerhub

  4- apagar a imagem docker (artefato) da instancia jenkins


#Plus !!!

Subir o jenkins em uma instancia aws atraves do terraform:

## instalar o terraform - no ubuntu 20

no site https://www.terraform.io/downloads.html confirme a versão que quer fazer o download


```
sudo apt install unzip -y

wget https://releases.hashicorp.com/terraform/0.15.4/terraform_0.15.4_linux_amd64.zip

unzip terraform_0.15.4_linux_amd64.zip

sudo mv terraform /usr/local/bin

rm -Rf terraform_0.15.4_linux_amd64*.*

terraform --version
```

ou tudo em uma linha apenas

```
sudo apt install unzip -y && wget https://releases.hashicorp.com/terraform/0.15.4/terraform_0.15.4_linux_amd64.zip && unzip terraform_0.15.4_linux_amd64.zip && sudo mv terraform /usr/local/bin && rm -Rf terraform_0.15.4_linux_amd64*.* && terraform --version
```


Rode um terraform init para baixar o provider na maquina e um terraform validate para verificar a sintaxe do arquivo main.tf

```
terraform init
terraform validate
```


Instalar o aws_cli e para isso é preciso que o python3 (ou superior) e o pip tambem esteja instalado na maquina.


Instalar aws-cli [https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html#awscli-install-linux-path](https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html#awscli-install-linux-path)

rode o comando abaixo para instalar o aws-cli na maquina

```
pip3 install awscli --upgrade --user
aws --version
```


**** falta colocar os comandos do terraform aqui e acertar o userdata para insalar o jenkins automaticamente na instancia --- informar as credenciais via export





  ![Topologia][topologia]

<!---[![Product Name Screen Shot][topologia]](https://example.com)




<!-- CONTACT -->
## Contatos

Hernani Soares - [@soaresnetoh](https://twitter.com/soaresnetoh) - soaresnetohl@gmail.com
[![LinkedIn][linkedin-shield]][linkedin-url]

Link do Projeto: [https://gitlab.com/soaresnetoh/desafiojenkins](https://gitlab.com/soaresnetoh/desafiojenkins)



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[topologia]: images/topologia.png
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/soaresnetoh
